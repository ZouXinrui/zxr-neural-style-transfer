# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, jsonify
from transfer.utils import list_images,list_video
from transfer.stylize import stylize
from transfer.stylize_video import style_video
from werkzeug.utils import secure_filename
import cv2
import base64
import numpy as np
import json
import sys
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
try:
    import psycopg2  # for heroku deploy
except:
    pass

ENCODER_WEIGHTS_PATH = '../transfer/vgg19_normalised.npz'
STYLE_WEIGHTS = [2.0]

# Uploaded directory for inferring images
INFERRING_CONTENT_DIR = 'static/trans_image/content'
INFERRING_STYLE_DIR = 'static/trans_image/style'
OUTPUTS_DIR = 'static/trans_image/output'

# Uploaded directory for inferring videos
VIDEO_CONTENT_DIR = 'static/trans_video/content'
VIDEO_STYLE_DIR = 'static/trans_video/style'
VIDEO_OUTPUTS_DIR = 'static/trans_video/output'


def remove(path):
    """Delete all files in folder"""
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)


def image_to_base64(image_np):
    """transfer image to base64 format"""
    img = image_np[..., ::-1] # RGB to BGR
    image = cv2.imencode('.png', img)[1]
    image_code = str(base64.b64encode(image))[2:-1]
    return image_code


def base64_to_image(base64_code):
    # base64 Decoding
    img_data = base64.b64decode(base64_code)
    # transfer to np
    img_array = np.fromstring(img_data, np.uint8)
    # opencv
    img = cv2.imdecode(img_array, cv2.COLOR_RGB2BGR)

    return img


@app.route('/')
def index():
    return render_template('tempo.html')


@app.route('/up_content', methods=['post'])
def up_content():
    """upload content image"""
    img = request.files.get('content')
    path = basedir+ "/"+INFERRING_CONTENT_DIR+ "/"
    if img:
        filename = secure_filename(img.filename)
        file_path = path + filename
        img.save(file_path)
    return jsonify()


@app.route('/up_style', methods=['post'])
def up_style():
    """upload style image"""
    img = request.files.get('style')
    path = basedir + "/"+ INFERRING_STYLE_DIR + "/"
    if img:
        filename = secure_filename(img.filename)
        file_path = path + filename
        img.save(file_path)
    return jsonify()


@app.route('/up_video_content', methods=['post'])
def up_video_content():
    """upload video"""
    video = request.files.get('video_content')
    path = basedir+ "/"+VIDEO_CONTENT_DIR+ "/"
    if video:
        filename = secure_filename(video.filename)
        file_path = path + filename
        video.save(file_path)
    return jsonify()


@app.route('/up_video_style', methods=['post'])
def up_video_style():
    """upload video style"""
    video = request.files.get('video_style')
    path = basedir + "/" + VIDEO_STYLE_DIR + "/"
    if video:
        filename = secure_filename(video.filename)
        file_path = path + filename
        video.save(file_path)
    return jsonify()


@app.route('/transfer_batch', methods=['post'])
def transfer_batch():
    # Get parameters from request data
    data  = request.get_data()
    data = data.decode()
    data = eval(data)
    # Controlling stylization degree
    param = data["param"]
    # Controlling AdaIN or WCT
    adain = data["adain"]
    param = float(param)
    adain = int(adain)
    ALPHA = [ param ]
    remove(OUTPUTS_DIR)

    # Use model
    model_path = '../transfer/model/model_wct/model_31.ckpt'

    try:
        content_imgs_path = list_images(INFERRING_CONTENT_DIR)
        style_imgs_path   = list_images(INFERRING_STYLE_DIR)

        for alpha in ALPHA:
            print('\n>>> Begin to stylize images with style weight: %.2f\n' % alpha)

            stylize(alpha, content_imgs_path, style_imgs_path, OUTPUTS_DIR,
                    ENCODER_WEIGHTS_PATH, model_path,adain=adain,
                    suffix='-' + str(alpha) + str(adain))

        print('\n>>> Successfully! Done all stylizing...\n')

        path_list = []
        # Return file list as ['house-flower-0.4.jpeg', 'house-flower-0.5.jpeg']
        tempo_list = os.listdir(OUTPUTS_DIR)
        for path in tempo_list:
            path = "/" + OUTPUTS_DIR + "/" + path
            path_list.append(path)
        message = path_list
        path_content = basedir + "/" +INFERRING_CONTENT_DIR + "/"
        # Clear content dir
        remove(path_content)
        path_style = basedir + "/" + INFERRING_STYLE_DIR + "/"
        # Clear style dir
        remove(path_style)
        # Send image path to frontend
        return json.dumps(message)

    except BaseException as e:
        print(e)
        message = 'wrong'
        # Send error message
        return json.dumps(message)


@app.route('/transfer_video', methods=['post'])
def transfer_video():
    """video style transfer function"""
    data  = request.get_data()
    param = data.decode()
    param = float(param)
    # Control stylization degree
    ALPHA = [ param ]
    remove(VIDEO_OUTPUTS_DIR)
    model_path = '../transfer/model/model_wct/model_31.ckpt'
    try:
        content_path = list_video(VIDEO_CONTENT_DIR)[0]
        style_path   = list_images(VIDEO_STYLE_DIR)[0]

        for alpha in ALPHA:

            style_video(content_path,VIDEO_OUTPUTS_DIR,style_path,ENCODER_WEIGHTS_PATH,model_path,alpha)

        print('\n>>> Successfully! Done all stylizing...\n')

        # Add file in output dir to the list
        path_list = []
        # Return file list as ['house-flower-0.4.jpeg', 'house-flower-0.5.jpeg']
        tempo_list = os.listdir(VIDEO_OUTPUTS_DIR)
        for path in tempo_list:
            path = "/" + VIDEO_OUTPUTS_DIR + "/" + path
            path_list.append(path)
        message = path_list
        path_content = basedir + "/" +VIDEO_CONTENT_DIR + "/"
        # Clear content dir
        remove(path_content)
        path_style = basedir + "/" + VIDEO_STYLE_DIR + "/"
        # Clear style dir
        remove(path_style)

        return json.dumps(message)

    except BaseException:
        message = 'wrong'
        return json.dumps(message)


# URL for rendering pages
@app.route('/gallery', methods=['GET', 'POST'])
def gallery():
    path_list = []
    path_name = []
    # Get image shown in this page
    RESULT_DIR = "static/images/result"
    tempo_list = os.listdir(RESULT_DIR)
    # Get the name of the image
    for path in tempo_list:
        path_img = "/" + RESULT_DIR + "/" + path
        name = os.path.splitext(path)[0]
        path_list.append(path_img)
        path_name.append(name)
    dic = dict(map(lambda x, y: [x, y], path_list, path_name))
    return render_template('gallary.html', dic=dic, title="Gallery")


@app.route('/impression', methods=['GET', 'POST'])
def impression():
    path_list = []
    path_name = []
    RESULT_DIR = "static/images/impression"
    tempo_list = os.listdir(RESULT_DIR)  # return file list
    for path in tempo_list:
        path_img = "/" + RESULT_DIR + "/" + path
        name = os.path.splitext(path)[0]
        path_list.append(path_img)
        path_name.append(name)
    dic = dict(map(lambda x, y: [x, y], path_list, path_name))
    return render_template('gallary.html', dic=dic, title="Impressionism")


@app.route('/fauvism', methods=['GET', 'POST'])
def fauvism():
    path_list = []
    path_name = []
    RESULT_DIR = "static/images/fauvism"
    tempo_list = os.listdir(RESULT_DIR)  # return file list
    for path in tempo_list:
        path_img = "/" + RESULT_DIR + "/" + path
        name = os.path.splitext(path)[0]
        path_list.append(path_img)
        path_name.append(name)
    dic = dict(map(lambda x, y: [x, y], path_list, path_name))
    return render_template('gallary.html', dic=dic, title="Fauvism")


@app.route('/chinese', methods=['GET', 'POST'])
def chinese():
    path_list = []
    path_name = []
    RESULT_DIR = "static/images/chinese"
    tempo_list = os.listdir(RESULT_DIR)  # return file list
    for path in tempo_list:
        path_img = "/" + RESULT_DIR + "/" + path
        name = os.path.splitext(path)[0]
        path_list.append(path_img)
        path_name.append(name)
    dic = dict(map(lambda x, y: [x, y], path_list, path_name))
    return render_template('gallary.html', dic=dic, title="Chinese Style")


@app.route('/abstract', methods=['GET', 'POST'])
def abstract():
    path_list = []
    path_name = []
    RESULT_DIR = "static/images/abstract"
    tempo_list = os.listdir(RESULT_DIR)  # return file list
    for path in tempo_list:
        path_img = "/" + RESULT_DIR + "/" + path
        name = os.path.splitext(path)[0]
        path_list.append(path_img)
        path_name.append(name)
    dic = dict(map(lambda x, y: [x, y], path_list, path_name))
    return render_template('gallary.html', dic=dic, title="Abstraction")


if __name__ == '__main__':
    app.run(debug=True)

