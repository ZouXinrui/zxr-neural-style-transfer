from os import listdir
from os.path import join
from PIL import Image

def list_images(directory):
    images = []
    for file in listdir(directory):
        name = file.lower()
        if name.endswith('.png'):
            images.append(join(directory, file))
        elif name.endswith('.jpg'):
            images.append(join(directory, file))
        elif name.endswith('.jpeg'):
            images.append(join(directory, file))
        elif name.endswith('.bmp'):
            images.append(join(directory, file))
    return images



def Image_PreProcessing(dir1,dir2,dir3):
    # 待处理图片存储路径

    paths = list_images(dir1)


    print('\nOrigin files number: %d\n' % len(paths))

    number = 1

    for path in paths:
        im = Image.open(path)
        # Resize图片大小，入口参数为一个tuple，新的图片大小
        imBackground = im.resize((256,256))
        # imBackground = im.crop((0,0,256,256))
        #处理后的图片的存储路径，以及存储格式
        imBackground.save(dir2+ str(number) +'.bmp','bmp')
        imBackground.save(dir3 + str(number) + '.png', 'png')
        number+=1

    print('\nDone.\n')



if __name__ == "__main__":
    Image_PreProcessing('../../../MS_COCO2/','../../../Compress/testBMP/','../../../Compress/testPNG/')
    # Image_PreProcessing('../../testImage/', '../../testImage/')
