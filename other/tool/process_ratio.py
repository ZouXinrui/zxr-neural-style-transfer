from transfer.tool.process_for_cut import list_images
import pandas as pd
import os

def ratio_compute():
    origin_path = list_images("../../../Compress/testBMP")
    compress_path = list_images("../../../Compress/testPNG")

    image_num = len(origin_path)
    print(image_num)

    ratios = []

    for image in range(image_num):
        before = os.path.getsize(origin_path[image])
        after = os.path.getsize(compress_path[image])
        ratio = after / before
        print(ratio)
        ratios.append(ratio)

    s= pd.Series(ratios)
    print(s)

    s.to_csv('Result.csv')

if __name__ == "__main__":
    ratio_compute()