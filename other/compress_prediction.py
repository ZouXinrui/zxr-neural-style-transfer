from keras.models import Sequential,load_model
from keras.layers import Dense,Flatten
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import time
import h5py
# import tensorflow as tf
# import os
#
# os.environ["CUDA_VISIBLE_DEVICES"] = '0'   #指定第一块GPU可用
# config = tf.ConfigProto()
# config.gpu_options.per_process_gpu_memory_fraction = 0.8  # 程序最多只能占用指定gpu50%的显存
# config.gpu_options.allow_growth = True      #程序按需申请内存
# sess = tf.Session(config = config)

FEATURE_DIR = "../../exp_data/features/vgg_feats.npy"
LABEL_DIR = "tool/Result.csv"
MODEL_SAVE_PATH = '../../exp_data/models/compress_prediction_no_bias_f_1.h5'

# ----------------------preparation of dataset--------------------------
dataset = np.load(FEATURE_DIR)
tempset= pd.read_csv(LABEL_DIR)
labelset = tempset['0']
labelset = labelset.astype('float32')
rand = np.random.permutation(len(dataset))
dataset = np.abs(dataset-dataset[rand])
labelset = np.abs(labelset-labelset[rand])

print(dataset.shape)
print(dataset.dtype)
print(labelset.shape)
print(labelset.dtype)

x_train, x_test, y_train, y_test = train_test_split(dataset, labelset, test_size = 0.2, random_state= 30)
x_train, x_valid, y_train, y_valid = train_test_split(x_train, y_train, test_size= 0.2, random_state= 13)

# weights = []
# for label in y_train:
#     compress = label
#     weight = abs(compress-0.66)
#     weights.append(weight)
#
# weights = np.array(weights)
# print(weights)


# # ----------------------------construct model-----------------------------
#
# batch_size = 3
# epochs = 10
# learning_rate = 0.001
#
# model = Sequential()
# model.add(Flatten(input_shape=(32,32,512)))
# model.add(Dense(128,activation='sigmoid',use_bias=False))
# model.add(Dense(128,activation='sigmoid',use_bias=False))
# temple = model.add(Dense(1,activation='sigmoid'))
# model.summary()
# model.compile(optimizer='adam',loss='mae')
#
# # --------------------------------training---------------------------------
#
# start_time = time.time()
# history = model.fit(x_train,y_train,batch_size=batch_size,epochs=epochs,verbose=2,
#                     validation_data=(x_valid,y_valid))
# trainTime = (time.time() - start_time)
# print('Training time = {}'.format(trainTime))
#
# model.save(MODEL_SAVE_PATH)
#
#
# # ------------------------------visualization-------------------------------
#
# # plot loss
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('Model loss')
# plt.ylabel('Loss')
# plt.xlabel('Epoch')
# plt.legend(['Train', 'Test'], loc='upper left')
# plt.show()

# -------------------------------evaluation---------------------------------
new_model=load_model(MODEL_SAVE_PATH)
# print(new_model.get_layer('dense_1').get_weights()[1])
# np.save('dense_1_weights.npy',new_model.get_layer('dense_1').get_weights()[0])
# np.save('dense_1_bias.npy',new_model.get_layer('dense_1').get_weights()[1])
# np.save('dense_2_weights.npy',new_model.get_layer('dense_2').get_weights()[0])
# np.save('dense_2_bias.npy',new_model.get_layer('dense_2').get_weights()[1])
# np.save('dense_3_weights.npy',new_model.get_layer('dense_3').get_weights()[0])
# np.save('dense_3_bias.npy',new_model.get_layer('dense_3').get_weights()[1])
#
# print(2)
# print(new_model.get_layer('dense_2').get_weights())
# print(3)
# print(new_model.get_layer('dense_3').get_weights())

# test_eval = new_model.evaluate(x_test,y_test)
for i in range(len(x_test)):
    item = x_test[i]
    item = item.reshape((1,32,32,512))
    result = new_model.predict(item)
    print(result)
# print('Test loss:',test_eval)
#
# new_model.predict


