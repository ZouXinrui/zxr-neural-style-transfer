#coding=utf-8
import os
import gc
import numpy as np
import tensorflow as tf
from transfer.encoder import Encoder
from transfer.utils import list_images, get_images
from scipy.linalg import norm
from keras.models import load_model

os.environ["CUDA_VISIBLE_DEVICES"] = '0'   #指定第一块GPU可用
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.7  # 程序最多只能占用指定gpu50%的显存
config.gpu_options.allow_growth = True      #程序按需申请内存
sess = tf.Session(config = config)

DATA_DIR = "../../Compress/testPNG"
encoder_weights_path ="vgg19_normalised.npz"
MODEL_SAVE_PATH = 'models/compress_prediction_no_bias_f_1.h5'


def get_featrues():
    images = list_images(DATA_DIR)
    unique_images = [images[i:i+30] for i in range(0,len(images),30)]
    batch_list=[]
    for unique_image in unique_images:
        batch = None
        for single_image in unique_image:
            img_list = get_images(single_image,256,256)
            img = img_list.reshape((1,256,256,3))
            if batch is None:
                batch = img
            else:
                batch = np.concatenate((batch,img))
        batch_list.append(batch)
    print(batch_list)

    with tf.Session() as sess:
        vgg = Encoder(encoder_weights_path)
        images = tf.placeholder(tf.float32, [None, None, None, 3])
        vgg.preprocess(images)
        enc_gen, enc_gen_layers = vgg.encode(images)
        number = 0
        for batch in batch_list:
            size = len(batch)
            print(size)
            number+=1
            print(number)
            batch = batch.reshape((size,256,256,3))
            sess.run(tf.global_variables_initializer())
            feature = sess.run(enc_gen_layers['relu4_1'], feed_dict={images: batch})  # feature of relu4_1
            print(feature.shape)
            vgg16_feats = np.save('features/vgg_feats' + str(number) + '.npy', feature)
            test = np.load('features/vgg_feats'+str(number)+'.npy')
            print(test)


def merge_npy(num):
    final = None
    for number in range(1,num):
        temp = np.load('features/vgg_feats'+str(number)+'.npy')
        print(len(temp))
        if final is None:
            final = temp
        else:
            final = np.concatenate((final,temp))
    np.save('features/vgg_feats_2.npy',final)


def getSingleFeature(image_path):
    with tf.Session() as sess:
        vgg = Encoder(encoder_weights_path)
        images = tf.placeholder(tf.float32, [None, None, None, 3])
        vgg.preprocess(images)
        enc_gen, enc_gen_layers = vgg.encode(images)
        img = get_images(image_path,256,256)
        sess.run(tf.global_variables_initializer())
        feature = sess.run(enc_gen_layers['relu4_1'], feed_dict={images: img})  # feature of relu4_1
    return feature


def prediction(image_path):
    imgFeature= getSingleFeature(image_path)
    model=load_model(MODEL_SAVE_PATH)
    print(model.predict(imgFeature))


def prediction_diff(img1,img2):
    img1f= getSingleFeature(img1)
    img2f = getSingleFeature(img2)
    imgFeature = img1f-img2f
    model = load_model(MODEL_SAVE_PATH)
    print("Result",model.predict(imgFeature))


if __name__ == "__main__":
    # get_featrues()
    # merge_npy(20)
    # prediction('../../tempoTest/1.png')
    # prediction('../../tempoTest/2.png')
    # prediction_diff('../../tempoTest/1.png', '../../tempoTest/2.png')
    # prediction_diff('../../tempoTest/2.png', '../../tempoTest/4.png')
    # prediction_diff('../../tempoTest/1.png', '../../tempoTest/4.png')
    print(getSingleFeature('../../tempoTest/1.png').shape)







