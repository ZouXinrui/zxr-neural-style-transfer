# Use a trained Image Transform Net to generate
# a style transferred image with a specific style

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

from transfer.style_transfer_net import StyleTransferNet
from transfer.utils import get_images, save_images,list_images

from os.path import join, exists, splitext
from scipy.misc import imread, imsave, imresize
from os import listdir, mkdir, sep
import numpy as np


def preprocess(image):
    if len(image.shape) == 3:  # Add batch dimension
        image = np.expand_dims(image, 0)
    return image / 255.  # Range [0,1]


def postprocess(image):
    return np.uint8(np.clip(image, 0, 1) * 255)


def stylize(contents_path, styles_path, output_dir, encoder_path, model_path,
            resize_height=None, resize_width=None, suffix=None):

    if isinstance(contents_path, str):
        contents_path = [contents_path]
    if isinstance(styles_path, str):
        styles_path = [styles_path]

    with tf.Graph().as_default(), tf.Session() as sess:
        # build the dataflow graph
        content = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='content')
        style   = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='style')

        stn = StyleTransferNet(encoder_path)

        output_image = stn.transform(1, content, style)

        sess.run(tf.global_variables_initializer())

        # restore the trained model and run the style transferring
        saver = tf.train.Saver()
        saver.restore(sess, model_path)

        outputs = []
        for content_path in contents_path:

            content_img = get_images(content_path,
                height=resize_height, width=resize_width)

            for style_path in styles_path:

                style_img   = get_images(style_path)

                result = sess.run(output_image,
                    feed_dict={content: content_img, style: style_img})

                outputs.append(result[0])

    save_images(outputs, contents_path, styles_path, output_dir, suffix=suffix)

    return outputs

def reconstruct(contents_path, output_dir, encoder_path, model_path,
            resize_height=None, resize_width=None, suffix=None):
    if isinstance(contents_path, str):
        contents_path = [contents_path]

    with tf.Graph().as_default(), tf.Session() as sess:
        # build the dataflow graph
        content = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='content')

        # create the style transfer net
        stn = StyleTransferNet(encoder_path)

        # switch RGB to BGR
        content_b = tf.reverse(content, axis=[-1])

        # preprocess image
        content_b = stn.encoder.preprocess(content_b)
        # encode image
        enc_c, enc_c_layers = stn.encoder.encode(content_b)
        # decode generate features back to image
        content_gen = stn.decoder.decode(enc_c)
        content_gen = stn.encoder.preprocess(content_gen)
        # deprocess image
        content_gen = stn.encoder.deprocess(content_gen)
        # switch BGR back to RGB
        content_gen = tf.reverse(content_gen, axis=[-1])
        # clip to 0..255
        output_image = tf.clip_by_value(content_gen, 0.0, 255.0)

        sess.run(tf.global_variables_initializer())

        # restore the trained model and run the style transferring
        saver = tf.train.Saver()
        saver.restore(sess, model_path)

        outputs = []
        for content_path in contents_path:
            content_img = get_images(content_path,
                                     height=resize_height, width=resize_width)

            result = sess.run(output_image,
                              feed_dict={content: content_img})

            outputs.append(result[0])


    data_idx = 0
    for content_path in contents_path:
            data = outputs[data_idx]
            data_idx += 1

            content_path_name, content_ext = splitext(content_path)

            content_name = content_path_name.split(sep)[-1]

            save_path = join(output_dir, '%s-%s%s' %
                             (content_name, suffix, content_ext))

            imsave(save_path, data)

    return outputs

# ----------------------------- 思考一下怎么改

def stylize_web(content_img, style_img, encoder_path, model_path,
            resize_height=None, resize_width=None, suffix=None):
    try:
        with tf.Graph().as_default(), tf.Session() as sess:
            # build the dataflow graph
            content = tf.placeholder(
                tf.float32, shape=(1, None, None, 3), name='content')
            style   = tf.placeholder(
                tf.float32, shape=(1, None, None, 3), name='style')

            stn = StyleTransferNet(encoder_path)

            output_image = stn.transform(content, style)

            sess.run(tf.global_variables_initializer())

            # restore the trained model and run the style transferring
            saver = tf.train.Saver()
            saver.restore(sess, model_path)

            result = sess.run(output_image,
                feed_dict={content: content_img, style: style_img})

    except Exception as ex:
        result = 'wrong'
    return result

if __name__ == '__main__':

    content_imgs_path = list_images('images/content')
    style_imgs_path = list_images('images/style')
    MODEL_SAVE_PATHS = [
        '../../exp_data/model/relu_4-2.ckpt'
    ]
    OUTPUTS_DIR = '../../exp_data/output_reconstruct3/'
    Output_dir = '../../exp_data/output_wct3/'
    encoder_path = 'vgg19_normalised.npz'

    for model_save_path in MODEL_SAVE_PATHS:
        print('\n>>> Begin to stylize images\n')

        reconstruct(content_imgs_path, OUTPUTS_DIR,
                encoder_path, model_save_path)

        stylize(content_imgs_path, style_imgs_path, Output_dir,
                encoder_path, model_save_path)

    print('\n>>> Successfully! Done all stylizing...\n')

