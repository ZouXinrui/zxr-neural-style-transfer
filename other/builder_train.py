# Train the Style Transfer Net

from __future__ import print_function

import numpy as np
import tensorflow as tf

from transfer.style_transfer_net import StyleTransferNet
from transfer.utils import get_train_images,sum_smooth,blur,list_images

# for training
TRAINING_CONTENT_DIR = '../../MS_COCO'
ENCODER_WEIGHTS_PATH = 'vgg19_normalised.npz'
LOGGING_PERIOD = 40

STYLE_WEIGHTS = [1.0]
MODEL_SAVE_PATHS = [
    'model-relu4/relu_4.ckpt',
    # 'model/vgg_relu_3.ckpt',
    # 'model/vgg_relu_2.ckpt',
    # 'model/vgg_relu_1.ckpt'
]
STYLE_LAYERS  = ['relu4_1']

# STYLE_LAYERS  = ('relu4_1','relu3_1', 'relu2_1', 'relu1_1')

LAYER_WEIGHTS = (1,1,1,1)

TRAINING_IMAGE_SHAPE = (256, 256, 3) # (height, width, color_channels)

tmp_save_path = "tempo_model/style_weight_3e0.ckpt"

EPOCHS = 4
EPSILON = 1e-5
BATCH_SIZE = 8
LEARNING_RATE = 1e-4
LR_DECAY_RATE = 5e-5
DECAY_STEPS = 1.0

feature_weight = 1
pixel_weight = 1
tv_weight = 0

def train(content_imgs_path, encoder_path, style_layer,
          model_save_path, debug=False, logging_period=100):
    if debug:
        from datetime import datetime
        start_time = datetime.now()

    # guarantee the size of content and style images to be a multiple of BATCH_SIZE
    num_imgs = len(content_imgs_path)
    content_imgs_path = content_imgs_path[:num_imgs]

    mod = num_imgs % BATCH_SIZE
    if mod > 0:
        print('Train set has been trimmed %d samples...\n' % mod)
        content_imgs_path = content_imgs_path[:-mod]

    # get the traing image shape
    HEIGHT, WIDTH, CHANNELS = TRAINING_IMAGE_SHAPE
    INPUT_SHAPE = (BATCH_SIZE, HEIGHT, WIDTH, CHANNELS)

    # create the graph
    with tf.Graph().as_default(), tf.Session() as sess:
        content = tf.placeholder(tf.float32, shape=INPUT_SHAPE, name='content')

        # ###########################################-------------------------------------generate img
        # create the style transfer net
        stn = StyleTransferNet(encoder_path)

        # get content feature (also：style feature)
        content_b = tf.reverse(content, axis=[-1]) # switch RGB to BGR
        content_b = stn.encoder.preprocess(content_b) # preprocess image
        enc_c, enc_c_layers = stn.encoder.encode(content_b) # encode image

        # decode generate features back to image
        content_gen = stn.decoder.decode(enc_c)
        content_gen = stn.encoder.preprocess(content_gen)
        content_gen = stn.encoder.deprocess(content_gen) # deprocess image
        content_gen = tf.reverse(content_gen, axis=[-1]) # switch BGR back to RGB
        # clip to 0..255
        content_gen_img = tf.clip_by_value(content_gen, 0.0, 255.0)
        # ###########################################-------------------------------------generate img

        # pass the generated_img to the encoder, and use the output compute loss
        content_gen_encode = tf.reverse(content_gen_img, axis=[-1])  # switch RGB to BGR
        content_gen_encode = stn.encoder.preprocess(content_gen_encode)  # preprocess image
        enc_gen, enc_gen_layers = stn.encoder.encode(content_gen_encode)

        # Feature loss between encodings of original & reconstructed
        feature_loss = feature_weight * tf.losses.mean_squared_error(enc_gen_layers[style_layer], enc_c_layers[style_layer])

        # # compute the style loss
        # style_layer_loss = []
        # for weight,layer in zip(LAYER_WEIGHTS,STYLE_LAYERS):
        #     enc_style_feat = enc_c_layers[layer]
        #     enc_gen_feat   = enc_gen_layers[layer]
        #
        #     meanS, varS = tf.nn.moments(enc_style_feat, [1, 2])
        #     meanG, varG = tf.nn.moments(enc_gen_feat,   [1, 2])
        #
        #     sigmaS = tf.sqrt(varS + EPSILON)
        #     sigmaG = tf.sqrt(varG + EPSILON)
        #
        #     l2_mean  = tf.reduce_sum(tf.square(meanG - meanS))
        #     l2_sigma = tf.reduce_sum(tf.square(sigmaG - sigmaS))
        #
        #     style_layer_loss.append(weight*(l2_mean + l2_sigma))
        #
        # style_loss = tf.reduce_sum(style_layer_loss)


        # Pixel reconstruction loss between decoded/reconstructed img and original
        pixel_loss = pixel_weight * tf.losses.mean_squared_error(content_gen_img, content) / 65025

        # Total Variation loss
        if tv_weight > 0:
            tv_loss = tv_weight * tf.reduce_mean(tf.image.total_variation(content_gen_img))
        else:
            tv_loss = tf.constant(0.)

        loss = feature_loss + pixel_loss + tv_loss

        # Training step
        global_step = tf.Variable(0, trainable=False)
        learning_rate = tf.train.inverse_time_decay(LEARNING_RATE, global_step, DECAY_STEPS, LR_DECAY_RATE)
        train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step=global_step)

        sess.run(tf.global_variables_initializer())

        # saver
        saver = tf.train.Saver(max_to_keep=10)

        ###### Start Training ######
        step = 0
        n_batches = int(len(content_imgs_path) // BATCH_SIZE)

        if debug:
            elapsed_time = datetime.now() - start_time
            start_time = datetime.now()
            print('\nElapsed time for preprocessing before actually train the model: %s' % elapsed_time)
            print('Now begin to train the model...\n')

        try:
            for epoch in range(EPOCHS):

                np.random.shuffle(content_imgs_path)

                for batch in range(n_batches):
                    # retrive a batch of content and style images
                    content_batch_path = content_imgs_path[batch*BATCH_SIZE:(batch*BATCH_SIZE + BATCH_SIZE)]
                    content_batch = get_train_images(content_batch_path, crop_height=HEIGHT, crop_width=WIDTH)

                    # run the training step
                    sess.run(train_op, feed_dict={content: content_batch})

                    step += 1

                    if step % 5000 == 0:
                        saver.save(sess, model_save_path, global_step=step, write_meta_graph=False)

                    if debug:
                        is_last_step = (epoch == EPOCHS - 1) and (batch == n_batches - 1)

                        if is_last_step or step == 1 or step % logging_period == 0:
                            elapsed_time = datetime.now() - start_time
                            _tv_loss, _feature_loss, _pixel_loss, _loss = sess.run([tv_loss,feature_loss, pixel_loss, loss],
                                feed_dict={content: content_batch})

                            print('step: %d,  total loss: %.3f,  elapsed time: %s' % (step, _loss, elapsed_time))
                            print('feature loss: %.3f' % (_feature_loss))
                            print('pixel loss: %.3f' % (_pixel_loss))
        except Exception as ex:
            saver.save(sess, model_save_path, global_step=step)
            print('\nSomething wrong happens! Current model is saved to <%s>' % tmp_save_path)
            print('Error message: %s' % str(ex))

        ###### Done Training & Save the model ######
        saver.save(sess, model_save_path)

        if debug:
            elapsed_time = datetime.now() - start_time
            print('Done training! Elapsed time: %s' % elapsed_time)
            print('Model is saved to: %s' % model_save_path)

if __name__ == '__main__':
    content_imgs_path = list_images(TRAINING_CONTENT_DIR)

    for model_save_path,style_layer in zip(MODEL_SAVE_PATHS,STYLE_LAYERS):
        print('\n>>> Begin to train the network \n')

        train(content_imgs_path, ENCODER_WEIGHTS_PATH,style_layer,
              model_save_path, logging_period=LOGGING_PERIOD, debug=True)

    print('\n>>> Successfully! Done all training...\n')
