from keras.models import Sequential,load_model
from keras.layers import Dense,Flatten,Conv2D,MaxPooling2D
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import time
from transfer.utils import get_images,list_images

PICTURE_DIR = "../../Compress/testBMP/"
LABEL_DIR = "tool/Result.csv"
MODEL_SAVE_PATH = '../../exp_data/models/compress_test.h5'

image_list = list_images(PICTURE_DIR)
images = get_images(image_list,256,256)

tempset= pd.read_csv(LABEL_DIR)
labelset = tempset['0']

x_train, x_test, y_train, y_test = train_test_split(images, labelset, test_size = 0.2, random_state= 30)
x_train, x_valid, y_train, y_valid = train_test_split(x_train, y_train, test_size= 0.2, random_state= 13)

batch_size = 30
epochs = 18
learning_rate = 0.005

model = Sequential()
model.add(Conv2D(32,kernel_size=(5,5),activation='relu',input_shape=(256,256,3),padding='same'))
model.add(MaxPooling2D((2,2),padding='same'))
model.add(Conv2D(32,kernel_size=(5,5),activation='relu',input_shape=(256,256,3),padding='same'))
model.add(MaxPooling2D((2,2),padding='same'))
model.add(Conv2D(64,kernel_size=(5,5),activation='relu',input_shape=(256,256,3),padding='same'))

model.add(Flatten())
model.add(Dense(128,activation='relu'))
model.add(Dense(128,activation='relu'))
model.add(Dense(1,activation='sigmoid'))

model.compile(optimizer='adam',loss='mae')

model.summary()

# --------------------------------training---------------------------------

start_time = time.time()
history = model.fit(x_train,y_train,batch_size=batch_size,epochs=epochs,verbose=2,
                    validation_data=(x_valid,y_valid))
trainTime = (time.time() - start_time)
print('Training time = {}'.format(trainTime))

model.save(MODEL_SAVE_PATH)


# ------------------------------visualization-------------------------------

# plot loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

test_eval = model.evaluate(x_test,y_test)
print('Test loss:',test_eval)