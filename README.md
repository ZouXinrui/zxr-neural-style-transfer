Neural Style Transfer
========

<h4>An explorarory software project to study the NST algorithms and provide user with an art creation assistant web application <h4 />

Click **[here](https://v.youku.com/v_show/id_XNDY1NTA2MzM5Ng==.html)** for the web application demo (web page overview and operation of batch image transfer) and **[here](https://v.youku.com/v_show/id_XNDY3MTEwNzY5Ng==.html )** for operation demo of video style transfer


# Example

<img src="https://gitlab.com/ZouXinrui/zxr-neural-style-transfer/raw/master/other/example_img/example1.JPG" width = "800"  alt="" align=center />


| Content trade-off | Style Interpolation |
| ------ | ------ |
| <img src="https://gitlab.com/ZouXinrui/zxr-neural-style-transfer/raw/master/other/example_img/tradeoff.gif" height = "300"  alt="" align=center /> | <img src="https://gitlab.com/ZouXinrui/zxr-neural-style-transfer/raw/master/other/example_img/style-inter.gif" height = "300"  alt="" align=center /> |


**Video**

- Click **[here](https://v.youku.com/v_show/id_XNDY1NzI1Njc1Mg==.html)** to see the transferred video demo


# Prerequisites

- [Microsoft COCO dataset](http://msvocds.blob.core.windows.net/coco2014/train2014.zip)
- [WikiArt dataset](https://www.kaggle.com/c/painter-by-numbers)
- Python 3.6

### Environment

##### Use pip tool to install the following libaries:

- Flask==1.1.1
- Keras==2.1.4
- numpy==1.16.0
- scipy==1.1.0
- opencv-python==4.2.0.32
- pandas==1.0.1
- Pillow==6.2.1
- tensorboard==1.6.0 
- tensorflow==1.6.0 (GPU vertion depended on CUDA 9.0)

### For video

- ffmpeg (click **[here](http://ffmpeg.org/)** to see the install instruction )

# Manual
>The implementation of this project mainly includes two parts of code. <br /> 
>The first is the Flask web app contained in the folder `"app"` <br /> 
>The second is the training and testing code contained in the folder `"transfer"`


### Run web application in app file `app/app.py`

The application integrates both batch image transfer and video transfer

```
python app.py
```
### Run models separetely in `transfer/main.py`

You can choose whether to train or stylize by changing the flag `"TRAINING"` (Default`"False"` -> for testing) in `transfer/main.py` and then:  
```
python main.py
```
#### Notice that you can futher control parameters as follow:

##### 1. For training:<br />

###### Prameters in `transfer/main.py`: <br />
  (1) **ENCODER_WEIGHTS_PATH** &nbsp; The path of weights file of the pre-trained VGG-19  (Default`"vgg19_normalised.npz"`)<br />
  (2) **TRAINING_CONTENT_DIR** &nbsp; The path of MS-COCO images dataset for training (Default `"../../MS_COCO/"`) <br />
  (3) **TRAINING_STYLE_DIR** &nbsp; The path of WikiArt images dataset for training (Default `"../../WikiArt/"`) <br />
  (4) **MODEL_SAVE_PATHS** &nbsp;  The path of checkpoint files of trained models (Default `"model/model_wct/model_31.ckpt"`) <br />
  
###### Prameters in `transfer/train.py`: <br />
  (5) **LOGDIR** &nbsp; Save the training data through tensorboard (Default `"./log/model_name"`) , and input it in terminal after training`tensorboard --logdir= YourLogDIR` <br />
  (6) **CONTINUE_DIR** &nbsp; Breakpoint continuation training <br />
  
##### 2. For stylizing:<br />
  
###### Prameters in `transfer/main.py`: <br />
  (1) **INFERRING_CONTENT_DIR** &nbsp; The path of content images (Default `"images/content"`)<br />
  (2) **INFERRING_STYLE_DIR** &nbsp; The path of style images (Default`"images/style"`)<br />
  (3) **OUTPUTS_DIR** &nbsp; The path of output images (Default `"images/content"`) <br />
  
##### 3. Further control: <br />

###### Prameters in `transfer/main.py`: <br />
  (1) **adain** &nbsp; Flag of Algorithms (WCT or Adain), 1 for AdaIN and 0 for WCT <br />
  (2) **ALPHA** &nbsp; Content Trade-off 
  
##### 4. Style Interpolation <br />

  (1) This function is not included in `transfer/main.py` but in `transfer/stylize.py` <br />
      Change the corresponding dir for Style Interpolation and run this file

##### 5. Video Style Transfer <br />

  (1) This function is not included in `transfer/main.py` but in `transfer/stylize_video.py` <br />
      Change the corresponding dir for Video Style Transfer and run this file



# Attention

**About Video Encodeing:**

* Video coding format is not suitable for all devices
     
* Please select the appropriate format conversion (MPEG is applicable)
     
* Please select H.264 to play on the webpage
      
**About Batch Download Function:**

*  This feature is not indispensabl. It is recommended to use chorme browser, and other browsers are not strictly tested.

*  The file is in ZIP format, please note to save the appropriate suffix.

**About Algorithms:**

* This project combines two algorithms mentioned in the following paper and implements it in Tensorflow 
1.  Huang et al. [Arbitrary Style Transfer in Real-time with Adaptive Instance Normalization](https://arxiv.org/pdf/1703.06868.pdf) [official [version](https://github.com/xunhuang1995/AdaIN-style) in Torch]
2.  Li et al. [Universal Style Transfer via Feature Transforms](https://arxiv.org/pdf/1705.08086.pdf) [official [version](https://github.com/Yijunmaverick/UniversalStyleTransfer) in Torch]


