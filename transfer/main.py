# Demo - train the style transfer network & use it to generate an image

from __future__ import print_function

from transfer.train import train
from transfer.stylize import stylize
from transfer.utils import list_images

# Flag to control whether training or testing
TRAINING = False

# Parameters for training
TRAINING_CONTENT_DIR = '../../MS_COCO'
TRAINING_STYLE_DIR = '../../WikiArt_Total'
ENCODER_WEIGHTS_PATH = 'vgg19_normalised.npz'
LOGGING_PERIOD = 40

STYLE_WEIGHTS = [1.0]

# 0 for using AdaIN in transfer layer and 1 for WCT
adain = 0

# Controlling stylization degree
ALPHA = [0.25,0.5,0.75,1.0]


MODEL_SAVE_PATHS = [
    'model/model_wct/model_31.ckpt',
]

# For inferring (stylize)
INFERRING_CONTENT_DIR = 'images/content'
INFERRING_STYLE_DIR = 'images/style'

# Give output directory
OUTPUTS_DIR = 'images/output'


def main():

    if TRAINING:

        # Get a list of images from directory
        contents = list_images(TRAINING_CONTENT_DIR)
        styles   = list_images(TRAINING_STYLE_DIR)

        # Trained model with different weights at one time
        for style_weight, model_save_path in zip(STYLE_WEIGHTS, MODEL_SAVE_PATHS):
            print('\n>>> Begin to train the network with the style weight: %.2f\n' % style_weight)

            train(style_weight, contents, styles, ENCODER_WEIGHTS_PATH, 
                  model_save_path, adain=adain, logging_period=LOGGING_PERIOD, debug=True)

        print('\n>>> Successfully! Done all training...\n')

    else:

        contents = list_images(INFERRING_CONTENT_DIR)
        styles   = list_images(INFERRING_STYLE_DIR)

        # Infer painting with different stylization degree at one time
        for alpha in ALPHA:
            print('\n>>> Begin to stylize images with style weight: %.2f\n' % alpha)
            model_save_path = MODEL_SAVE_PATHS[0]

            stylize(alpha, contents, styles, OUTPUTS_DIR,
                    ENCODER_WEIGHTS_PATH, model_save_path, adain,
                    suffix='-' + str(alpha))

        print('\n>>> Successfully! Done all stylizing...\n')



if __name__ == '__main__':
    main()

