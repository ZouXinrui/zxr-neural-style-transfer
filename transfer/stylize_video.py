# Use a trained Image Transform Net to transfer
# a video into a specific style by using
# ffmpeg to process video to image

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

import os, random, subprocess, shutil, time
from transfer.style_transfer_net import StyleTransferNet
from transfer.utils import get_images,save_img

tmp_dir = '_____fns_frames_%s/' % random.randint(0,99999)


def style_video(in_path,out_path,style_img,encoder_path,model_path,alpha):

    # Create needed dirs
    in_dir = os.path.join(tmp_dir, 'input')
    out_dir = os.path.join(tmp_dir, 'sytlized')
    if not os.path.exists(in_dir):
        os.makedirs(in_dir)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    style_files = [style_img]
    print(style_files)

    # Construct ffmpeg command line
    in_args = [
        'ffmpeg',
        '-i', in_path,
        '%s/frame_%%d.png' % in_dir
    ]

    subprocess.call(" ".join(in_args), shell=True)
    base_names = os.listdir(in_dir)
    in_files = [os.path.join(in_dir, x) for x in base_names]
    out_files = [os.path.join(out_dir, x) for x in base_names]

    with tf.Graph().as_default(), tf.Session() as sess:
        # Build the dataflow graph
        content = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='content')
        style   = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='style')

        stn = StyleTransferNet(encoder_path)

        # Add style to content img
        output_image = stn.transform(1, content, style, alpha)

        # Make img size from 1xHxWxC to HxWxC
        output_image = tf.squeeze(output_image, 0)

        sess.run(tf.global_variables_initializer())

        # Restore the trained model and run the style transferring
        saver = tf.train.Saver()
        saver.restore(sess, model_path)

        s = time.time()

        content_prefix, content_ext = os.path.splitext(in_path)
        content_prefix = os.path.basename(content_prefix)

        try:
            for style_fullpath in style_files:
                style_img = get_images(style_fullpath)
                style_prefix, _ = os.path.splitext(style_fullpath)
                style_prefix = os.path.basename(style_prefix)

                # Generate output path name
                out_v = os.path.join(out_path, '{}_{}{}'.format(content_prefix, style_prefix, ".mp4"))

                print("OUT:", out_v)
                if os.path.isfile(out_v) is True:
                    print("SKIP", out_v)
                    continue

                for in_f, out_f in zip(in_files, out_files):
                    print('{} -> {}'.format(in_f, out_f))
                    content_img = get_images(in_f)
                    stylized = sess.run(output_image,
                                          feed_dict={content: content_img, style: style_img})
                    save_img(out_f, stylized)

                # Setup frame rate
                fr = 30
                # construct ffmpeg command line
                # Note: video coding is not suitable for all devices
                # Please select the appropriate format conversion (MPEG is applicable)
                # Please select H.264 to play on the webpage

                out_args = [
                        'ffmpeg',
                        '-i', '%s/frame_%%d.png' % out_dir,
                        '-f', 'mp4',
                        '-q:v', '0',
                        '-vcodec', 'h264',
                        '-r', str(fr),
                              '"' + out_v + '"'
                ]
                print(out_args)

                subprocess.call(" ".join(out_args), shell=True)
                print('Video at: %s' % out_v)

                # Remove temperate directory
                shutil.rmtree(tmp_dir)

            print('Processed in:', (time.time() - s))

        except Exception as e:
            print("EXCEPTION: ", e)


# Video Style Transfer testing
if __name__ == '__main__':
    # Stylization degree
    alpha = 0.5

    # Directory that video saved
    out_path = '../../exp_data/outvideo'

    # Input video path (single video)
    in_video = '../../video/lake.mp4'

    # Input style image (single image)
    style_img = 'images/style/3.png'

    encoder_path = 'vgg19_normalised.npz'
    model_path = '../../exp_data/model_wct/model_31.ckpt'
    style_video(in_video,out_path,style_img,encoder_path,model_path,alpha)

