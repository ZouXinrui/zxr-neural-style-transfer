# Code for Architecture as follows:
# Encoder - Transfer Layer - Decoder
# Input content and style image (np form) and output transferred image

import tensorflow as tf

from transfer.encoder import Encoder
from transfer.decoder import Decoder
from transfer.ops import AdaIN,wct_tf_batch
from other.attention import attention


class StyleTransferNet(object):

    def __init__(self, encoder_weights_path):
        self.encoder = Encoder(encoder_weights_path)
        self.decoder = Decoder()

    def transform(self, batch, content, style, alpha, adain=0):
        # Switch RGB to BGR
        content = tf.reverse(content, axis=[-1])
        style   = tf.reverse(style,   axis=[-1])

        # Preprocess image
        content = self.encoder.preprocess(content)
        style   = self.encoder.preprocess(style)

        # Encode image
        enc_c, enc_c_layers = self.encoder.encode(content)
        enc_s, enc_s_layers = self.encoder.encode(style)

        # ######## Attention Layer #############
        # enc_c = attention(enc_c)
        # enc_s = attention(enc_s)

        self.encoded_content_layers = enc_c_layers
        self.encoded_style_layers   = enc_s_layers

        # Algorithm choosing
        if adain == 0:
            # pass the encoded images to WCT layer
            target_features = wct_tf_batch(batch, enc_c, enc_s, alpha)
        else:
            target_features = AdaIN(enc_c, enc_s, alpha)

        self.target_features = target_features

        # Decode target features back to image
        generated_img = self.decoder.decode(target_features)

        # Deprocess image
        generated_img = self.encoder.deprocess(generated_img)

        # Switch BGR back to RGB
        generated_img = tf.reverse(generated_img, axis=[-1])

        # Clip to 0..255
        generated_img = tf.clip_by_value(generated_img, 0.0, 255.0)

        return generated_img

    def transform_two(self, batch, content, style1, style2, alpha, weight):
        # Switch RGB to BGR
        content = tf.reverse(content, axis=[-1])
        style1  = tf.reverse(style1,  axis=[-1])
        style2  = tf.reverse(style2,  axis=[-1])

        # Preprocess image
        content = self.encoder.preprocess(content)
        style1   = self.encoder.preprocess(style1)
        style2 = self.encoder.preprocess(style2)

        # Encode image
        enc_c, enc_c_layers = self.encoder.encode(content)
        enc_s1, enc_s1_layers = self.encoder.encode(style1)
        enc_s2, enc_s2_layers = self.encoder.encode(style2)

        # Pass the encoded images to WCT layer
        target_features1 = wct_tf_batch(batch, enc_c, enc_s1, alpha)
        target_features2 = wct_tf_batch(batch, enc_c, enc_s2, alpha)

        # Style Interpolated
        target_features = weight*target_features1 + (1-weight)*target_features2

        self.target_features = target_features

        # Reconstruct target features back to image
        generated_img = self.decoder.decode(target_features)

        # Deprocess image
        generated_img = self.encoder.deprocess(generated_img)

        # Switch BGR back to RGB
        generated_img = tf.reverse(generated_img, axis=[-1])

        # Clip to 0..255
        generated_img = tf.clip_by_value(generated_img, 0.0, 255.0)

        return generated_img

