# Use a trained Image Transform Net to generate
# a style transferred image with a specific style

from datetime import datetime

import tensorflow as tf
from transfer.utils import list_images

config = tf.ConfigProto()
config.gpu_options.allow_growth = True


from transfer.style_transfer_net import StyleTransferNet
from transfer.utils import get_images, save_images
from os.path import join, exists, splitext
from scipy.misc import imread, imsave, imresize
from os import listdir, mkdir


def stylize(alpha, contents_path, styles_path, output_dir, encoder_path, model_path, adain,
            resize_height=None, resize_width=None, suffix=None):

    if isinstance(contents_path, str):
        contents_path = [contents_path]
    if isinstance(styles_path, str):
        styles_path = [styles_path]

    with tf.Graph().as_default(), tf.Session(config=config) as sess:
        # Build the dataflow graph
        content = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='content')
        style   = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='style')

        stn = StyleTransferNet(encoder_path)

        output_image = stn.transform(1, content, style, alpha, adain)

        sess.run(tf.global_variables_initializer())

        # Restore the trained model and run the style transferring
        saver = tf.train.Saver()
        saver.restore(sess, model_path)

        outputs = []
        for content_path in contents_path:

            content_img = get_images(content_path,
                height=resize_height, width=resize_width)

            for style_path in styles_path:
                start_time = datetime.now()
                style_img   = get_images(style_path)
                result = sess.run(output_image, 
                    feed_dict={content: content_img, style: style_img})

                elapsed_time = datetime.now() - start_time

                print('Time is %s' % elapsed_time)
                outputs.append(result[0])

    save_images(outputs, contents_path, styles_path, output_dir, suffix=suffix)

    return outputs


# Two Style Interpolate
def stylize_two(alpha, contents_path, styles_path, output_dir, encoder_path, model_path, weight,
            resize_height=None, resize_width=None, suffix=None):

    if isinstance(contents_path, str):
        contents_path = [contents_path]
    if isinstance(styles_path, str):
        styles_path = [styles_path]

    with tf.Graph().as_default(), tf.Session() as sess:
        # build the dataflow graph
        content = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='content')
        style_1 = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='style1')
        style_2 = tf.placeholder(
            tf.float32, shape=(1, None, None, 3), name='style2')

        stn = StyleTransferNet(encoder_path)

        output_image = stn.transform_two(1, content, style_1, style_2, alpha, weight)

        sess.run(tf.global_variables_initializer())

        # Restore the trained model and run the style transferring
        saver = tf.train.Saver()
        saver.restore(sess, model_path)

        outputs = []
        for content_path in contents_path:

            content_img = get_images(content_path,
                height=resize_height, width=resize_width)

            for i in range(0,len(styles_path)-1):

                style_img1   = get_images(styles_path[i])
                style_img2   = get_images(styles_path[i+1])

                result = sess.run(output_image,
                    feed_dict={content: content_img, style_1: style_img1, style_2: style_img2})

                outputs.append(result[0])

    if not exists(output_dir):
        mkdir(output_dir)

    for i in range(len(outputs)):
        save_path = join(output_dir, '%s-%s%s' %
                         (str(i), suffix, ".jpg"))
        imsave(save_path, outputs[i])

    return outputs


# Two style interpolation
if __name__ == '__main__':
    # Stylization degree
    alpha = 0.9

    # Directory setting
    out_path = '../../exp_data/images/out_two'
    content_img = '../../exp_data/images/content'
    style_img = '../../exp_data/images/style'
    encoder_path = 'vgg19_normalised.npz'
    model_path = '../../exp_data/model_wct/model_31.ckpt'

    # Weight between style 1 and style 2
    WEIGHT = [0, 0.2, 0.4, 0.6, 0.8, 1]

    # Get image from directory
    content_imgs_path = list_images(content_img)
    style_imgs_path = list_images(style_img)

    for weight in WEIGHT:
        print('\n>>> Begin to stylize images with style weight: %.2f\n' % weight)
        stylize_two(alpha, content_imgs_path, style_imgs_path, out_path, encoder_path, model_path, weight,
                    suffix='-' + str(weight))

    print('\n>>> Successfully! Done all stylizing...\n')







