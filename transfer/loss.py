# All loss functions used in this project
# including Content Loss, Style Loss (BN loss and Gram loss) and TV loss

import tensorflow as tf
EPSILON = 1e-5


# Compute Gram - version 1
def gram(layer):
    shape = tf.shape(layer)
    num_images = shape[0]
    width = shape[1]
    height = shape[2]
    num_filters = shape[3]
    filters = tf.reshape(layer, tf.stack([num_images, -1, num_filters]))
    grams = tf.matmul(filters, filters, transpose_a=True) / tf.to_float(width * height * num_filters)
    return grams


# Compute Gram - version 2
def gram_matrix(input_tensor):
    result = tf.linalg.einsum('bijc,bijd->bcd', input_tensor, input_tensor)
    input_shape = tf.shape(input_tensor)
    num_locations = tf.cast(input_shape[1]*input_shape[2], tf.float32)
    return result/(num_locations)


# Gram style loss between generated image and style image
def style_gram_loss(enc_style_feat,enc_gen_feat):
    Gs = gram_matrix(enc_style_feat)
    Gt = gram_matrix(enc_gen_feat)
    return tf.nn.l2_loss(Gs - Gt) * 2 / 2040


# 4-Nearest-Neighbour difference
def high_pass_x_y(image):
    x_var = image[:,:,1:,:] - image[:,:,:-1,:]
    y_var = image[:,1:,:,:] - image[:,:-1,:,:]
    return x_var, y_var


# 4-Nearest-Neighbour tv loss
def total_variation_loss(image):
    x_deltas, y_deltas = high_pass_x_y(image)
    return tf.reduce_mean(abs(x_deltas)) + tf.reduce_mean(abs(y_deltas))


# 8-Nearest-Neighbour difference
def high_pass(image):
    x_var = image[:,:,1:,:] - image[:,:,:-1,:]
    y_var = image[:,1:,:,:] - image[:,:-1,:,:]
    xy_var = image[:, 1:, 1:, :] - image[:, :-1, :-1, :]
    yx_var = image[:, 1:, :-1, :] - image[:, :-1, 1:, :]
    return x_var, y_var, xy_var, yx_var


# 8-Nearest-Neighbour tv loss
def total_variation_loss_eight(image):
    x_deltas, y_deltas, xy_deltas,yx_deltas = high_pass(image)
    return tf.reduce_mean(abs(x_deltas)) + tf.reduce_mean(abs(y_deltas))\
           + tf.reduce_mean(abs(xy_deltas)) + tf.reduce_mean(abs(yx_deltas))


# BN loss
def style_layer_loss(enc_style_feat,enc_gen_feat):
    meanS, varS = tf.nn.moments(enc_style_feat, [1, 2])
    meanG, varG = tf.nn.moments(enc_gen_feat, [1, 2])

    sigmaS = tf.sqrt(varS + EPSILON)
    sigmaG = tf.sqrt(varG + EPSILON)

    l2_mean = tf.reduce_sum(tf.square(meanG - meanS))
    l2_sigma = tf.reduce_sum(tf.square(sigmaG - sigmaS))
    return l2_mean + l2_sigma


# Calculate the total variation of image
def sum_smooth(images):
    images = tf.pad(images, [[0, 0], [1, 1], [1, 1], [0, 0]], "SYMMETRIC")

    center = images[:, 1: - 1, 1: - 1, :]

    up_left   = images[:, 0:- 2, 0: - 2, :] - center
    up_right  = images[:, 0: - 2, 2:, :] - center
    up_center = images[:, 0: - 2, 1: - 1, :] - center
    left      = images[:, 1: - 1, 0: - 2, :] - center
    right     = images[:, 1:- 1, 2:, :] - center
    low_left  = images[:, 2:, 0: - 2, :] - center
    low_right = images[:, 2:, 2:, :] - center
    low_center= images[:, 2:, 1: -1 , :] - center

    result  = (abs(up_left) + abs(up_right) + abs(up_center) +
                abs(left) + abs(right) + abs(low_center) +
                abs(low_left) + abs(low_right))/2040 # divided by 8*255

    result = tf.reduce_mean(result,axis=0)

    result1 = tf.reduce_sum(result)

    return result1


# Smooth loss - difference between Mean filter
def blur(image_list):
    images = tf.pad(image_list, [[0, 0], [1, 1], [1, 1], [0, 0]], "SYMMETRIC")

    center = images[:, 1: - 1, 1: - 1, :]

    up_left = images[:, 0: - 2, 0: - 2, :]
    up_right = images[:, 0: - 2, 2:, :]
    up_center = images[:, 0: - 2, 1: - 1, :]
    left = images[:, 1: - 1, 0: - 2, :]
    right = images[:, 1: - 1, 2:, :]
    low_left = images[:, 2:, 0: - 2, :]
    low_right = images[:, 2:, 2:, :]
    low_center = images[:, 2:, 1: -1, :]

    result = (up_left + up_right + up_center +
              left + right + low_center +
              low_left + low_right + center) / 9

    result1 = sum_smooth(image_list)-sum_smooth(result)

    return result1