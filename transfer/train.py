# Train the Style Transfer Net

from __future__ import print_function

import numpy as np
import tensorflow as tf

from transfer.style_transfer_net import StyleTransferNet
from transfer.utils import get_train_images
from transfer.loss import style_layer_loss,style_gram_loss,\
    total_variation_loss_eight,total_variation_loss,sum_smooth,blur

# VGG layer uesed to calculate loss
STYLE_LAYERS  = ('relu1_1', 'relu2_1', 'relu3_1', 'relu4_1')

LAYER_WEIGHTS = (1,1,1,1)

CONTINUE_DIR = "" # Breakpoint continuation training

TRAINING_IMAGE_SHAPE = (256, 256, 3) # (height, width, color_channels)

# Save current parameters to model if there are any error
tmp_save_path = "../../exp_data/tempo_model/style_weight_3e0.ckpt"

# TensorBoard Log Saving"
LOGDIR = "./log/model_name"

# Training parameters
EPOCHS = 5
EPSILON = 1e-5
BATCH_SIZE = 8
LEARNING_RATE = 1e-4
LR_DECAY_RATE = 5e-5
DECAY_STEPS = 1.0
SMOOTH_LOSS_WIGHT = 0.0
CONTENT_LOSS_WIGHT = 1.0
TV_LOSS_WIGHT = 0.0

# Algorithm Controlling
alpha = 1.0
GRAM = 1


def train(style_weight, content_imgs_path, style_imgs_path, encoder_path, 
          model_save_path, adain, debug=False, logging_period=100):
    if debug:
        from datetime import datetime
        start_time = datetime.now()

    # Guarantee the size of content and style images to be a multiple of BATCH_SIZE
    num_imgs = min(len(content_imgs_path), len(style_imgs_path))
    content_imgs_path = content_imgs_path[:num_imgs]
    style_imgs_path   = style_imgs_path[:num_imgs]
    mod = num_imgs % BATCH_SIZE
    if mod > 0:
        print('Train set has been trimmed %d samples...\n' % mod)
        content_imgs_path = content_imgs_path[:-mod]
        style_imgs_path   = style_imgs_path[:-mod]

    # Get the traing image shape
    HEIGHT, WIDTH, CHANNELS = TRAINING_IMAGE_SHAPE
    INPUT_SHAPE = (BATCH_SIZE, HEIGHT, WIDTH, CHANNELS)

    # Create the graph
    with tf.Graph().as_default(), tf.Session() as sess:
        with tf.name_scope('input'):
            content = tf.placeholder(tf.float32, shape=INPUT_SHAPE, name='content')
            style   = tf.placeholder(tf.float32, shape=INPUT_SHAPE, name='style')

            # Show current input ：images in databases
            tf.summary.image('content', content, 3)
            tf.summary.image('style', style, 3)

        # Create the style transfer net
        stn = StyleTransferNet(encoder_path)

        with tf.name_scope('result'):
            # Getting the generated_img
            generated_img = stn.transform(BATCH_SIZE, content, style, alpha, adain)

            tf.summary.image('result', generated_img, 3)

        # Get the target feature maps which is the output of AdaIN
        target_features = stn.target_features

        # Get the feature map of generated_img
        generated_img = tf.reverse(generated_img, axis=[-1])  # switch RGB to BGR
        generated_img = stn.encoder.preprocess(generated_img) # preprocess image
        enc_gen, enc_gen_layers = stn.encoder.encode(generated_img)

        with tf.name_scope("loss_function"):
            # Compute the content loss
            content_loss = tf.reduce_sum(tf.reduce_mean(tf.square(enc_gen - target_features), axis=[1, 2]))*CONTENT_LOSS_WIGHT

            # Copute the blur loss
            if SMOOTH_LOSS_WIGHT == 0.0:
                smooth_loss = tf.constant(0.)
            else:
                content_smooth = sum_smooth(content)
                style_smooth = sum_smooth(style)
                gen_img_smooth = sum_smooth(generated_img)
                smooth_loss = 400000 * (abs(style_smooth - gen_img_smooth)) * SMOOTH_LOSS_WIGHT

            # Whether use gram loss or BN loss
            if GRAM == 1:
                # Gram_Loss
                style_loss = 0
                for layer in STYLE_LAYERS:
                     enc_style_feat = stn.encoded_style_layers[layer]
                     enc_gen_feat   = enc_gen_layers[layer]
                     style_gram = style_gram_loss(enc_style_feat,enc_gen_feat)
                     style_loss += style_gram
            else:
                # Compute the BN style loss
                layer_loss = []
                for weight,layer in zip(LAYER_WEIGHTS,STYLE_LAYERS):
                    enc_style_feat = stn.encoded_style_layers[layer]
                    enc_gen_feat   = enc_gen_layers[layer]

                    s_layer_loss= style_layer_loss(enc_style_feat,enc_gen_feat)

                    layer_loss.append(weight*s_layer_loss)

                style_loss = tf.reduce_sum(layer_loss)

            # Compute total variation loss
            if TV_LOSS_WIGHT == 0.0:
                tv_loss = tf.constant(0.)
            else:
                tv_loss = total_variation_loss(generated_img) * TV_LOSS_WIGHT

            # Compute the total loss
            loss = content_loss + style_weight * style_loss + smooth_loss + tv_loss

            # Summary variables to Tensorboard
            tf.summary.scalar('loss', loss)
            tf.summary.scalar('style_loss', style_loss)
            tf.summary.scalar('content_loss', content_loss)

        # Training step
        with tf.name_scope("train_step"):
            global_step = tf.Variable(0, trainable=False)
            learning_rate = tf.train.inverse_time_decay(LEARNING_RATE, global_step, DECAY_STEPS, LR_DECAY_RATE)
            train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step=global_step)

        sess.run(tf.global_variables_initializer())

        # Saver
        saver = tf.train.Saver(max_to_keep=10)

        '''
        # Breakpoint continuation training
        ckpt = tf.train.get_checkpoint_state(CONTINUE_DIR)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)  # restore newest model in model_checkpoint_path
            print("Model restored...")
        else:
            print('No Model')
        '''

        merged = tf.summary.merge_all()
        writer = tf.summary.FileWriter(LOGDIR, sess.graph)

        ###### Start Training ######
        step = 0
        n_batches = int(len(content_imgs_path) // BATCH_SIZE)

        if debug:
            elapsed_time = datetime.now() - start_time
            start_time = datetime.now()
            print('\nElapsed time for preprocessing before actually train the model: %s' % elapsed_time)
            print('Now begin to train the model...\n')

        try:
            for epoch in range(EPOCHS):

                np.random.shuffle(content_imgs_path)
                np.random.shuffle(style_imgs_path)

                for batch in range(n_batches):
                    # Retrive a batch of content and style images
                    content_batch_path = content_imgs_path[batch*BATCH_SIZE:(batch*BATCH_SIZE + BATCH_SIZE)]
                    style_batch_path   = style_imgs_path[batch*BATCH_SIZE:(batch*BATCH_SIZE + BATCH_SIZE)]

                    content_batch = get_train_images(content_batch_path, crop_height=HEIGHT, crop_width=WIDTH)
                    style_batch   = get_train_images(style_batch_path,   crop_height=HEIGHT, crop_width=WIDTH)

                    # Run the training step
                    sess.run(train_op, feed_dict={content: content_batch, style: style_batch})

                    step += 1

                    if step % 5000 == 0:
                        saver.save(sess, model_save_path, global_step=step, write_meta_graph=False)

                    if debug:

                        is_last_step = (epoch == EPOCHS - 1) and (batch == n_batches - 1)

                        if is_last_step or step == 1 or step % logging_period == 0:
                            elapsed_time = datetime.now() - start_time
                            summary,_tv_loss,_smooth_loss, _content_loss, _style_loss, _loss = sess.run([merged, tv_loss, smooth_loss, content_loss, style_loss, loss],
                                feed_dict={content: content_batch, style: style_batch})

                            print('step: %d,  total loss: %.4f,  elapsed time: %s' % (step, _loss, elapsed_time))
                            print('content loss: %.4f' % (_content_loss))
                            print('smooth loss: %.4f' % (_smooth_loss))
                            print('total variation loss: %.4f' % (_tv_loss))
                            print('style loss  : %.4f,  weighted style loss: %.3f\n' % (_style_loss, style_weight * _style_loss))

                            writer.add_summary(summary, step)

        except Exception as ex:
            saver.save(sess, model_save_path, global_step=step)
            print('\nSomething wrong happens! Current model is saved to <%s>' % tmp_save_path)
            print('Error message: %s' % str(ex))

        ###### Done Training & Save the model ######
        saver.save(sess, model_save_path)

        writer.close()

        if debug:
            elapsed_time = datetime.now() - start_time
            print('Done training! Elapsed time: %s' % elapsed_time)
            print('Model is saved to: %s' % model_save_path)

